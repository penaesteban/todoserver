import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;



public class View {
	
	
	protected Label LblTitel;
	protected  Button btnStart;
	protected Stage stage;
	private Model model;
	protected TextField  txtPort;
	protected Label LblPort;
	protected TextArea txtOutput;
	protected Button btnStop;
	
	
	public View(Stage stage, Model model) {
		
		this.stage = stage;
		this.model = model;
		
		
		LblTitel = new Label("ToDo Server");
		btnStart = new Button("Start");
		txtPort = new TextField ("50002");
		LblPort = new Label("Port");
		txtOutput = new TextArea("");
		btnStop = new Button("Stop");
		
		
		GridPane root = new GridPane();
		root.add(LblTitel, 0, 1);
		root.add(btnStart, 0,2);
		root.add(btnStop,1,2);
		root.add(LblPort, 0,3);
		root.add(txtPort, 0,4);
		root.add(txtOutput, 0,5);
		
		
		Scene scene = new Scene(root);
		
		stage.setScene(scene);
		stage.setHeight(800);
		stage.setWidth(800);
		
	
	
				
	}

	public void  start() {
		stage.show();
		
	}
	
	public Stage getStage() {
		return stage;
	
	}

	
	
}
