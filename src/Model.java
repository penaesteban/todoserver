import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Model {

	
	private ServerSocket listener;
	private User user = new User();
	private ToDo toDo = new ToDo();
	protected final ObservableList<Client> clients = FXCollections.observableArrayList();
	private boolean stop = false;
	
	
	public void startServer(int port) {
		
		try {
			listener = new ServerSocket(port, 10, null);
			Runnable r = new Runnable() {
				@Override
				public void run() {
					while (!stop) {
						try {
							Socket socket = listener.accept();
							Client client = new Client(socket, Model.this);
							clients.add(client);
						} catch (Exception e) {
							e.printStackTrace(System.out);
						}
					}
				}
			};
			Thread t = new Thread(r, "ServerSocket");
			t.start();
		} catch (IOException e) {
			e.printStackTrace(System.out);
		}
	
	}
	
	public void stopServer() {
		for (Client c : clients) c.stop();

		stop = true;
		if (listener != null) {
			try {
				listener.close();
			} catch (IOException e) {
				// Uninteresting
			}
		}
	}
	
	
	/*
	public void readMessage() {
		
		System.out.println("readMessage()");
		
		try {
			Socket socket = listener.accept();
			System.out.println("listener.accept()");
			this.socket = socket;
			
			BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			System.out.println("socket.getInputStream()");
			String msgText = in.readLine(); // Will wait here for complete line
			
			System.out.println(msgText);
			
			parse(msgText);
		}
		
		catch(Exception e) {
			System.out.println("Exception in readMessage()");
			e.printStackTrace(System.out);
		}
		
		
	}
	*/
	
	public void parse(String msgText, Socket socket) {

		System.out.println("parse()");
		
		String[] parts = msgText.split("\\|");
		
		System.out.println("Split: " + parts[0]);
		System.out.println(Arrays.toString(parts));
		
		ToDoItem item;
		int index;
		
		switch(parts[0]) {
		
			case "Ping":
				send("Result|true", socket);
				break;
				
			case "CreateLogin":
				Credentials credentials = new Credentials(parts[1], parts[2]);
				if(user.CreateLogin(credentials)) 
					send("Result|true", socket);
				else
					send("Result|false", socket);
				break;
				
			case "Login":
				String token = user.Login(parts[1], parts[2]);
				if(token != null ) 
					send("Result|true|" + token, socket );
				else
					send("Result|false", socket);
				break;
				
			case "Logout":
				user.Logout();
				send("Result|true", socket);
				break;
				
			case"CreateToDo":
				 item = new ToDoItem(parts[2],parts[4],""  ,parts[3], user.getLoggedInUser());
				toDo.AddItem(item);
				send("Result|true", socket);
				break;
				
			case"GetToDo":
				index = Integer.parseInt(parts[2]);
				item = toDo.GetItem(index);
				if(user.isLoggedIn() && user.toDoBelongsToUser(item)) 
					send("Result|true|"+ index +"|"+ item.getTitel()+"|"+ item.getPriority()+"|"+ item.getBeschreibung(), socket);
				else 
					send("Result|false", socket);
				break;
				
			case"DeleteToDo":
				index = Integer.parseInt(parts[2]);
				item = toDo.GetItem(index);
				if(user.isLoggedIn() && user.toDoBelongsToUser(item))  {
					toDo.DeleteItem(index);
					send("Result|true", socket);
				}
				else 
					send("Result|false", socket);
				break;
				
			case"ListToDos":
				ArrayList<ToDoItem>items = toDo.GetAll();
				String ids = "";
				for (int i= 0; i < items.size();i++) {
					if(user.isLoggedIn() && user.toDoBelongsToUser(items.get(i)))  {
							ids += i + ",";				
					}
				}
				send("Result|true|" + ids, socket);
				break;
				
				
			default:
				send("Result|false", socket);
					
		}
	}
	
	
	public void send(String msgText, Socket socket){

		System.out.println("send()");
		
		try {
			
			OutputStreamWriter out = new OutputStreamWriter(socket.getOutputStream());
			out.write(msgText + "\n");
			out.flush();			
			
		}
		catch(Exception e) {
			e.printStackTrace(System.out);
		}
		
		
	}
}
