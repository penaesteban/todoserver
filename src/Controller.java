

public class Controller {
	private Model model;
	private View view;

	public Controller(Model model, View view) {
		this.model = model;
		this.view = view;

	
		
		view.btnStart.setOnAction(e ->{
			int port = Integer.parseInt(view.txtPort.getText());
			model.startServer (port);
			
		});
		
		view.btnStop.setOnAction(e ->{
			model.stopServer() ;
			
		});
		
		

		view.stage.setOnCloseRequest(event -> model.stopServer());

		
	}
}