import java.io.Serializable;

public class Credentials implements Serializable{
	
	private static final long serialVersionUID = 1L;
	String username;
	String password;
	String token;
	
	
	public Credentials (String username, String password) {
		this.username = username;
		this.password = password;
	}
	
	public String getUsername(){
		return username;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
}
