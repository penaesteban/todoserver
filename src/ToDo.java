import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;


public class ToDo {
	
ArrayList <ToDoItem> Items = new ArrayList <ToDoItem>();
	
	// Methode für die Erstellung des To Do Items => CREATE 
	
	public void AddItem (ToDoItem NewItem) {
		Items.add(NewItem);
		SaveToFile();
	}
	
	// Methode fürs READ = > GET MEthode
	
	public ToDoItem GetItem (int index) {
		return Items.get(index);
		
	}
	
	// Methode fürs Update => 
	
	public void UpdateItem (int index, ToDoItem NewItem) {
		Items.set(index,NewItem);
		SaveToFile();
	}
	
	//Methode, um alle Itesm aus dem Aarys zu erhalten 
	
	public ArrayList<ToDoItem> GetAll (){
		return Items;
	}
	
	// Methode fürs Deleten 
	
		
	
	public void DeleteItem(int index) {
		Items.remove(index);
		SaveToFile();
	}
	
	public int GetCount() {
		return Items.size();
		
	}
	
	public void SaveToFile() {
		
		try {
			FileOutputStream fos = new FileOutputStream("Todo.txt");
		    ObjectOutputStream oos = new ObjectOutputStream(fos);
		    oos.writeObject(Items);
		    oos.close();
		}
		catch(Exception e) {			
			Alert a = new Alert(AlertType.ERROR, "Fehler beim Speichern: "+ e.getMessage());
			a.showAndWait();
		}
	}
	
	public void ReadFromFile() {
		try {
			FileInputStream fis = new FileInputStream("Todo.txt");
			ObjectInputStream ois = new ObjectInputStream(fis);
			
			Items = (ArrayList<ToDoItem>)ois.readObject();
			ois.close();
			
			
		}
		catch(Exception e){
			Alert a = new Alert(AlertType.ERROR, "Fehler beim Aufrufen der Daten: "+ e.getMessage());
			a.showAndWait();
			
		}
	}

}
