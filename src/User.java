import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;


public class User {
	ArrayList<Credentials>credentialsList = new ArrayList<Credentials>();
	Credentials loggedInUser = null;
	
	public boolean UserExists(Credentials newCredentials) {
		
		System.out.println("Check for user: " + newCredentials.getUsername());
		
		for( Credentials credentials : credentialsList) {
			
			System.out.println("User in List: " + credentials.getUsername());
			
			if( credentials.getUsername().equals(newCredentials.getUsername())) {
				return true;
			}
					
		}
		
		return false;
	}

	public boolean CreateLogin(Credentials newCredentials) {
		
		if(UserExists(newCredentials)) {
			System.out.println("User exists!");
			return false;
		}
		else {
			System.out.println("User exists not!");
			credentialsList.add(newCredentials);
			return true;
		}	
	}
	
	public String Login(String username, String password) {
		
		for(int i = 0; i < credentialsList.size() ; i++) {
			
			System.out.println("Check user id" + i);
			
			if(username.equals(credentialsList.get(i).getUsername()) && password.equals(credentialsList.get(i).getPassword())) {
				String token = UUID.randomUUID().toString();
				Credentials credentials = credentialsList.get(i);
				credentials.setToken(token);
				credentialsList.set(i, credentials);
				loggedInUser = credentials;
				return token;
				
			}
			
		}
		
		return null;
		
	}
	
	public void Logout() {
		
		for(int i = 0; i < credentialsList.size() ; i++) {
			
			if(loggedInUser.getUsername().equals(credentialsList.get(i).getUsername()) && loggedInUser.getPassword().equals(credentialsList.get(i).getPassword())) {
				Credentials credentials = credentialsList.get(i);
				credentials.setToken("");
				credentialsList.set(i, credentials);
				loggedInUser = null;
				return;
			}
			
		}
		
	}

	public Credentials getLoggedInUser() {
	return loggedInUser;
}

	public boolean isLoggedIn() {
		
		if(loggedInUser == null) {
			return false;
		}
		else {
			return true;
		}
		
	}

	public boolean toDoBelongsToUser(ToDoItem item) {
		
		if(item.getCredentials().getUsername().equals(loggedInUser.getUsername())) {
			return true;
		}
		else {
			return false;
		}
	}
}
