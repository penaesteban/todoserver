import java.io.Serializable;
import java.time.LocalDateTime;

public class ToDoItem implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String Titel;
	private String Beschreibung;
	private String Ort;
	private String Priority;
	private Credentials Credentials;
	
	public ToDoItem ( String Titel, String Beschreibung, String Ort, String Priority, Credentials Credentials) {	
		this.Titel= Titel;
		this.Beschreibung = Beschreibung;
		this.Ort = Ort;
		this.Priority = Priority;
		this.Credentials = Credentials;
	}
	
	public String getTitel() {
		return Titel;
	}
	
	public String getBeschreibung() {
		return Beschreibung;
	}
	

	
	public String getOrt() {
		return Ort;
	}
	public String getPriority() {
		return Priority;
	}
	
	public Credentials getCredentials() {
		return Credentials;
	}
	
	
	public void  SetTitle (String Titel) {
		this.Titel = Titel;
	}
	
	public void SetBeschreibung (String Beschreibung) {
		this.Beschreibung = Beschreibung;
	}
	
	
	public void setOrt( String Ort) {
		this.Ort = Ort;
	}
	
	public void setPriority(String Priority) {
		this.Priority = Priority;
	}
	
	public void setCredentials(Credentials Credentials) {
		this.Credentials = Credentials;
	}
	
	
}
