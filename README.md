# ToDoServer

Dieses Projekt gehört zum Miniprojekt Nr.2 beim Modul Software Engineering.

Das Programm wurde als Einzelarbeit durch Esteban Andres Pena Zhina programmiert.

Funktionalität:

Server verfügt über die vorgegeben Messagesarten ( client -> server), welche vom Professor im PDF "To-Do Miniproject" definiert wurden.

- **MessageType**: Ping ; **Erwartetes Ergebnis**: Result|True
- **MessageType**: CreateLogin|username|password ; **Erwartetes Ergebnis**: Result|True
- **MessageType**: Login|username|password ; **Erwartetes Ergebnis**: Result|True|Token => vom Server generiert
- **MessageType**: Logout ; **Erwartetes Ergebnis**: Result|True

**To-Do MessageTypes**

- **MessageType + Eingabe von Client**: CreateToDo|Token vom Server|Titel|Prority|Beschreibung ; **Erwartetes Ergebnis**: Result|True
- **MessageType + Eingabe von Client**: GetToDo|Token vom Server|index ; **Erwartetes Ergebnis**: Result|True|index|Titel|Priority|Beschreibung
- **MessageType + Eingabe von Client**: DeleteToDo|index der ToDo Notiz ; **Erwartetes Ergebnis**: Result|True
- **MessageType + Eingabe von Client**: ListToDos  ; **Erwartetes Ergebnis**: Result|Lists of Index


**Beim Crash oder Einfrieren des Servers:**
Server und Client schliessen und neustarten
